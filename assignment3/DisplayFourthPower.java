package assignment3;
import java.util.Scanner;
public class DisplayFourthPower {

	public static void main(String[] args) {
		System.out.print("Enter the side length value: ");

		Scanner s=new Scanner(System.in);

		int n=s.nextInt();

		double ans=Math.pow(n,4);

		System.out.println("Square : "+n*n);

		System.out.println("Cube : "+n*n*n);

		System.out.print("Fourth power is : "+ans);
	}

}
